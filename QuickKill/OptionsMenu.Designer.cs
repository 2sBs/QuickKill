﻿namespace QuickKill
{
    partial class OptionsMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.performanceCounter1 = new System.Diagnostics.PerformanceCounter();
            this.boxConfirmKill = new System.Windows.Forms.CheckBox();
            this.boxMessage = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.performanceCounter1)).BeginInit();
            this.SuspendLayout();
            // 
            // boxConfirmKill
            // 
            this.boxConfirmKill.AutoSize = true;
            this.boxConfirmKill.Location = new System.Drawing.Point(12, 12);
            this.boxConfirmKill.Name = "boxConfirmKill";
            this.boxConfirmKill.Size = new System.Drawing.Size(122, 17);
            this.boxConfirmKill.TabIndex = 0;
            this.boxConfirmKill.Text = "Confirm process kill?";
            this.boxConfirmKill.UseVisualStyleBackColor = true;
            // 
            // boxMessage
            // 
            this.boxMessage.AutoSize = true;
            this.boxMessage.Location = new System.Drawing.Point(12, 35);
            this.boxMessage.Name = "boxMessage";
            this.boxMessage.Size = new System.Drawing.Size(242, 17);
            this.boxMessage.TabIndex = 1;
            this.boxMessage.Text = "Pop up notification once app has been killed?";
            this.boxMessage.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(91, 58);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // OptionsMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 94);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.boxMessage);
            this.Controls.Add(this.boxConfirmKill);
            this.Name = "OptionsMenu";
            this.Text = "Options";
            this.Load += new System.EventHandler(this.OptionsMenu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Diagnostics.PerformanceCounter performanceCounter1;
        private System.Windows.Forms.CheckBox boxConfirmKill;
        private System.Windows.Forms.CheckBox boxMessage;
        private System.Windows.Forms.Button btnSave;
    }
}